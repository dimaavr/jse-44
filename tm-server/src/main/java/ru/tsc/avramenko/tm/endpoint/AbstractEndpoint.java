package ru.tsc.avramenko.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator){
        this.serviceLocator = serviceLocator;
    }

}